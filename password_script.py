import sys

def main():
  if len(sys.argv) != 2:
    print("Invalid args")
    return
  password = sys.argv[1]
  builder = 0
  for c in password:
    builder += ord(c)
  if builder == 316 and len(password) == 4 and ord(password[3]) == 100:
    print("correct")
  else:
    print("incorrect")

if __name__ == "__main__":
  main()

